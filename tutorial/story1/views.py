from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Steven Kusuman' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 12, 14) #TODO Implement this, format (Year, Month, Date)
npm = 1706028676 # TODO Implement this
img_src = 'https://avatars3.githubusercontent.com/u/1707491?s=280&v=4'
hobby = 'sleep eat pwn repeat'
# Create your views here.
def story_1(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'img_src': img_src, 'hobby': hobby}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
