from django import forms

from .models import JadwalPribadi

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class JadwalPribadiForm(forms.ModelForm):

    class Meta:
        model = JadwalPribadi
        fields = '__all__'
        widgets = {
            'date': DateInput(),
            'time': TimeInput(),
        }