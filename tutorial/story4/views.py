from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from .forms import JadwalPribadiForm
from .models import JadwalPribadi

# Create your views here.
def index(request):
    return render(request, 'index.html')

def projects(request):
    return render(request, 'projects.html')

def articles(request):
    return render(request, 'articles.html')

def register(request):
    return render(request, 'register.html')

def jadwal(request):
    form = JadwalPribadiForm()
    jadwal_pribadis = JadwalPribadi.objects.all()

    if(request.POST):
        form = JadwalPribadiForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwal.html')
        else:
            return redirect('jadwal.html')
    else:
        return render(request, 'jadwal.html', {'form': form
            , 'jadwal_pribadis': jadwal_pribadis})

@csrf_exempt
def delete_jadwal(request):
    if(request.POST):
        jadwal_pribadis = JadwalPribadi.objects.get(pk=int(request.POST.get('pk'))).delete()
        return redirect('jadwal.html')

@csrf_exempt
def delete_jadwal_all(request):
    JadwalPribadi.objects.all().delete()
    return redirect('jadwal.html')