from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def welcome(request):
    form = StatusForm()
    statuses = Status.objects.all()
    if(request.POST):
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            return redirect('/')
    return render(request, 'welcome-page.html', {'form': form
            , 'statuses': statuses})

