from django.test import TestCase, Client
from .models import Status

# Testing welcome page
class Story6WelcomePageTestCase(TestCase):

    def setUp(self):
        self.client = Client()


    def test_if_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


    def test_hello(self):
        response = self.client.get('/')
        self.assertIn(b'Hello, Apa kabar?', response.content)


    def test_create_status_success(self):
        response = self.client.post('/', data={'content':'a'*200})
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Status.objects.all())
        response = self.client.get('/')
        self.assertIn(b'a'*200, response.content)


    def test_create_status_fail(self):
        response = self.client.post('/', data={'content':'a'*301})
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Status.objects.all())
        response = self.client.get('/')
        self.assertNotIn(b'a'*301, response.content)
