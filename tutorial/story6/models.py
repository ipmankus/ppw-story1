from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    content = models.CharField(max_length=300)
    date_created = models.DateTimeField(default=timezone.now)