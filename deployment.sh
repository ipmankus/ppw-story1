#!/bin/bash
cd tutorial
python manage.py makemigrations
python manage.py migrate
python manage.py migrate --run-syncdb
